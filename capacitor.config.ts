import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'MCalc',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
