import { Component, OnInit, Input } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
export class ExploreContainerComponent implements OnInit {
  height = 0;
  heightUnits: 'in' | 'cm' | 'mm' = 'mm';
  heightInMm = 0;
  thicknessInMm = 0;
  lengthInMm = 0;
  length = 0;
  lengthUnits: 'in' | 'cm' | 'mm' = 'mm';
  thickness = 0;
  thicknessUnits: 'in' | 'cm' | 'mm' = 'mm';
  density = 0;
  weight = 0;
  ratePerSheet = 0;
  ratePerKg = 0;

  constructor(public toastController: ToastController) {}

  ngOnInit() {}

  async calculateRate() {
    switch (this.heightUnits) {
      case 'mm':
        this.heightInMm = this.height;
        break;
      case 'cm':
        this.heightInMm = this.height * 10;
        break;
      case 'in':
        this.heightInMm = this.height * 25.4;
        break;
    }

    switch (this.lengthUnits) {
      case 'mm':
        this.lengthInMm = this.length;
        break;
      case 'cm':
        this.lengthInMm = this.length * 10;
        break;
      case 'in':
        this.lengthInMm = this.length * 25.4;
        break;
    }

    switch (this.thicknessUnits) {
      case 'mm':
        this.thicknessInMm = this.thickness;
        break;
      case 'cm':
        this.thicknessInMm = this.thickness * 10;
        break;
      case 'in':
        this.thicknessInMm = this.thickness * 25.4;
        break;
    }
    this.weight =
      this.lengthInMm * this.heightInMm * this.thicknessInMm * this.density;
    this.ratePerSheet = this.ratePerKg / (1000 / this.weight);
  }

  async showInvalidDensityError() {
    const toast = await this.toastController.create({
      message: 'Invalid Density',
      duration: 5000,
    });
    toast.present();
  }

  clear() {
    this.thickness = undefined;
    this.length = undefined;
    this.height = undefined;
    this.density = undefined;
    this.ratePerKg = undefined;
    this.thicknessUnits = 'mm';
    this.lengthUnits = 'mm';
    this.heightUnits = 'mm';
  }
}
